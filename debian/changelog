libdata-uuid-perl (1.227-1) unstable; urgency=medium

  * Team upload.
  * debian/watch: keep only one URL.
  * Import upstream version 1.227.
    - eliminated use of state/node files in temp directory,
      addressing CVE-2013-4184
      Closes: #718949
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Apr 2024 15:43:19 +0200

libdata-uuid-perl (1.226-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 14:09:56 +0000

libdata-uuid-perl (1.226-2) unstable; urgency=medium

  [ Jenkins ]
  * Update standards version to 4.5.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 09:54:33 +0100

libdata-uuid-perl (1.226-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.

  [ Joenio Marques da Costa ]
  * Import upstream version 1.226
  * Add myself to d/copyright

  [ gregor herrmann ]
  * Bump debhelper-compat to 13.

 -- Joenio Marques da Costa <joenio@joenio.me>  Fri, 06 Nov 2020 00:12:32 +0100

libdata-uuid-perl (1.224-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.

  [ Florian Schlichting ]
  * Import upstream version 1.224
  * Drop cdbs using dh-make-perl refresh
  * Add myself to Uploaders and d/copyright
  * Add Rules-Requires-Root: no
  * Use secure copyright-format URI
  * Add upstream metadata
  * Update d/watch to version 4, using ANY_VERSION and ARCHIVE_EXT

 -- Florian Schlichting <fsfs@debian.org>  Fri, 27 Mar 2020 18:34:47 +0100

libdata-uuid-perl (1.220-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + improve chances it'll work on Android.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to use cgit web frontend.

  [ Jonas Smedegaard ]
  * Bump debhelper compatibility level to 9.
  * Add metacpan URL as alternative watch file entry.
  * Declare compliance with Debian Policy 3.9.6.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage of packaging to include current year.
  * Modernize git-buildpackage config: Avoid git- prefix.
  * Override lintian regarding build-depending unversioned on debhelper.
  * Override lintian regarding license in License-Reference field.
    See bug#786450.

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 04 Aug 2015 15:06:17 +0200

libdata-uuid-perl (1.219-2) unstable; urgency=low

  * Fix replace and break versions of libossp-uuid-perl providing
    Data::UUID shim.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 20 Jul 2013 22:52:37 +0200

libdata-uuid-perl (1.219-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#717315.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 19 Jul 2013 12:29:04 +0200
